<?php

//https://pt.stackoverflow.com/questions/190015/como-fazer-um-request-com-curl

if (isset($_POST['apiFornecedor']))
{
  $urlAPI = $_POST['apiFornecedor'];

  if ($_POST['complVisualizar'])
  {
    $urlAPIPath = $_POST['apiFornecedor'] . $_POST['complVisualizar'];

    $resposta = curlFornecedor($urlAPIPath);
  }
  elseif ($_POST['complCalcular'])
  {
    $urlAPIPath = $_POST['apiFornecedor'] . $_POST['complCalcular'] . '/' . $_POST['valorCalcular'];

    $resposta = curlFornecedor($urlAPIPath);
  }
  echo gettype($resposta) . "<br>";

  echo "<pre>";
  echo json_encode($resposta);
  echo "</pre>";
}

function curlFornecedor($urlAPIPath)
{
  // Iniciamos a função do CURL:
  $ch = curl_init($urlAPIPath);

  curl_setopt_array($ch, [

    // Equivalente ao -X:
    CURLOPT_CUSTOMREQUEST => 'GET',

    // Equivalente ao -H:
    CURLOPT_HTTPHEADER => [
      'JsonOdds-API-Key: yourapikey'
    ],

    // Permite obter o resultado
    CURLOPT_RETURNTRANSFER => 1,
  ]);

  $resposta = json_decode(curl_exec($ch), true);
  curl_close($ch);

  return $resposta;
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Testes slim</title>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</head>
<body>
  <section class="section">
    <div class="container">



      <form class="" action="index.php" method="post">
        <div class="field">
          <label class="label">Local da API</label>
          <div class="control">
            <input type="text" class="input" name="apiFornecedor" value="http://localhost/php-lampp/_exemplos/api-fornecedor/">
          </div>
        </div>


        <!-- VISUALIZAR JSON -->
        <fieldset class="box">
          <legend class="label has-text-centered">Visualizar</legend>
          <div class="field">
            <label class="label">Complemento da URL</label>
            <div class="control">
              <input type="text" name="complVisualizar" class="input">
            </div>
            <p class="help">visualizar</p>
          </div>
        </fieldset>



        <!-- CALCULO COM JSON -->
        <fieldset class="box">
          <legend class="label has-text-centered">Calculo</legend>
          <div class="field">
            <label class="label">Complemento da URL</label>
            <div class="control">
              <input type="text" name="complCalcular" class="input">
            </div>
            <p class="help">calcular</p>
          </div>
          <div class="field">
            <label class="label">Numero a ser calculado</label>
            <div class="control">
              <input type="number" name="valorCalcular" class="input">
            </div>
          </div>
        </fieldset>


        <!-- CRUD - CREATE -->

        <!-- CRUD - READ -->

        <!-- CRUD - UPDATE -->

        <!-- CRUD - DELETE -->

        <input type="submit" class="button is-info" name="">



      </form>
    </div>
  </section>
</body>
</html>
